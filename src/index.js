import shell from 'shelljs';
import Enquirer from 'enquirer';
import fnetYaml from '@fnet/yaml';

const { prompt } = Enquirer;

/**
 * Loads and parses a YAML file.
 *
 * @param {string} filepath - The path to the YAML file.
 * @returns {Object|null} - The parsed YAML object or null if file not found.
 */
const loadYamlFile = async (filepath) => {
  try {
    const result = await fnetYaml({ file: filepath });
    return result?.parsed;
  } catch (err) {
    console.warn(`Warning: Could not read or parse ${filepath}.`);
    return null;
  }
};

/**
 * Runs a shell command and optionally suppresses the output.
 *
 * @param {string} command - The shell command to execute.
 * @param {boolean} verbose - Whether to display command output.
 * @returns {boolean} - Returns true if command succeeded, false otherwise.
 */
const runShellCommand = (command, verbose = true) => {
  const result = shell.exec(command, { silent: !verbose });
  return result.code === 0;
};

/**
 * Checks if the current directory is already a git repository.
 *
 * @returns {boolean} - True if the directory is a git repository, false otherwise.
 */
const isGitRepository = () => {
  return shell.exec('git rev-parse --is-inside-work-tree', { silent: true }).code === 0;
};

/**
 * Retrieves the current remote URL for the origin.
 *
 * @returns {string|null} - The current remote URL for the origin, or null if not set.
 */
const getRemoteUrl = () => {
  const result = shell.exec('git remote get-url origin', { silent: true });
  if (result.code === 0) {
    return result.stdout.trim();
  }
  return null;
};

/**
 * Compares two repository URLs, ignoring protocol differences.
 *
 * @param {string} url1 - The first repository URL.
 * @param {string} url2 - The second repository URL.
 * @returns {boolean} - True if the URLs refer to the same repository, false otherwise.
 */
const areUrlsEquivalent = (url1, url2) => {
  // Remove the protocol and compare domain + repo name
  const normalizeUrl = (url) => url.replace(/(^\w+:|^)\/\//, '').replace('git@', '').replace(':', '/');
  return normalizeUrl(url1) === normalizeUrl(url2);
};

/**
 * Extracts the repository name from a GitHub repository URL.
 * 
 * @param {string} repoUrl - The GitHub repository URL.
 * @returns {string} - The repository name.
 */
const getGitHubRepoName = (repoUrl) => {
  try {
    // Handle SSH URL (git@github.com:username/repository.git)
    if (repoUrl.startsWith('git@')) {
      const repoName = repoUrl.split(':')[1]; // Extracts 'username/repository.git'
      return repoName.replace('.git', '');    // Remove '.git' extension
    }

    // Handle HTTPS URL (https://github.com/username/repository.git)
    const url = new URL(repoUrl);
    return url.pathname.replace(/(^\/|\.git$)/g, '');  // Remove leading '/' and '.git'

  } catch (error) {
    throw new Error('Invalid repository URL format.');
  }
};

/**
 * Initializes a git repository, adds a remote URL, and pushes the initial commit.
 * The repo URL is either taken from args or parsed from 'node.yaml' or 'flow.yaml' file.
 * If a repository already exists, notifies the user and exits or proceeds accordingly.
 *
 * @param {Object} args - The arguments for the module.
 * @param {string} [args.repo_url] - The repository URL. If not provided, it's parsed from the YAML files.
 * @param {boolean} [args.verbose=true] - Whether to run commands in verbose mode.
 */
export default async (args) => {
  try {
    const verbose = args.verbose !== false; // default to true if not provided

    // Parse the YAML files to find the repo URL if not provided in args
    let repoUrl = args.repo_url;
    if (!repoUrl) {
      const parsedYaml = await loadYamlFile('node.yaml') || await loadYamlFile('flow.yaml');

      // Check both YAML files for the repo URL
      repoUrl = parsedYaml?.repo?.url;
    }

    if (!repoUrl) {
      throw new Error('Repository URL not provided and could not be found in YAML files.');
    }

    // Check if the current directory is already a git repository
    if (isGitRepository()) {
      const currentRemoteUrl = getRemoteUrl();

      if (currentRemoteUrl) {
        // If a remote URL already exists, compare with the provided one ignoring protocol differences
        if (areUrlsEquivalent(currentRemoteUrl, repoUrl)) {
          console.log('The repository is already initialized with the same remote URL.');
          return;
        } else {
          // Remote URL exists but is different from the provided one
          console.log(`The repository already has a remote URL (${currentRemoteUrl}), which is different from the provided one (${repoUrl}).`);
          const { updateRemote } = await prompt({
            type: 'confirm',
            name: 'updateRemote',
            message: 'Do you want to update the remote URL to the new one?'
          });

          if (!updateRemote) {
            console.log('No changes were made to the remote URL.');
            return;
          }

          // Update the remote URL
          if (!runShellCommand(`git remote set-url origin ${repoUrl.replace('https://', 'git@').replace('/', ':')}`, verbose)) {
            throw new Error('Failed to update the remote origin URL.');
          }

          console.log('Remote origin URL updated successfully.');
        }
      } else {
        // No remote URL is set, add it
        console.log('This directory is already a Git repository but has no remote origin.');
        const { addRemote } = await prompt({
          type: 'confirm',
          name: 'addRemote',
          message: 'Do you want to add the provided repository URL as the remote origin?'
        });

        if (!addRemote) {
          console.log('Operation cancelled by the user.');
          return;
        }

        // Check if the repo is for GitHub and create it using GitHub CLI if applicable
        if (repoUrl.includes('github.com')) {
          const repoName = getGitHubRepoName(repoUrl);

          // Check if GitHub CLI is installed
          if (shell.which('gh')) {
            console.log(`Creating GitHub repository ${repoName} using GitHub CLI...`);
            if (!runShellCommand(`gh repo create ${repoName} --public --source=. --remote=origin`, verbose)) {
              throw new Error('Failed to create GitHub repository.');
            }
          } else {
            console.log('GitHub CLI (gh) is not installed. Please ensure the repository exists on GitHub.');
          }
        }
        else {
          // Add the remote URL
          if (!runShellCommand(`git remote add origin ${repoUrl.replace('https://', 'git@').replace('/', ':')}`, verbose)) {
            throw new Error('Failed to add remote origin.');
          }
        }
        
        console.log('Remote origin added successfully.');

        // After adding remote, ask if the user wants to push the existing commits
        const { pushChanges } = await prompt({
          type: 'confirm',
          name: 'pushChanges',
          message: 'Do you want to push the existing commits to the remote repository?'
        });

        if (pushChanges) {
          if (!runShellCommand('git push --set-upstream origin main', verbose)) {
            throw new Error('Failed to push the changes to the remote repository.');
          }
          console.log('Changes pushed to the remote repository successfully.');
        } else {
          console.log('Push operation skipped.');
        }
      }
    } else {
      // If the directory is not a git repository, initialize and add remote
      const { confirm, branch } = await prompt([
        {
          type: 'confirm',
          name: 'confirm',
          message: 'Do you want to proceed with initializing the git repository and pushing the initial commit?'
        },
        {
          type: 'input',
          name: 'branch',
          message: 'Enter the branch name (default: main):',
          initial: 'main'
        }
      ]);

      if (!confirm) {
        console.log('Operation cancelled by the user.');
        return;
      }


      // Initialize the git repository and set the remote URL
      if (!runShellCommand(`git init --initial-branch=${branch}`, verbose)) {
        throw new Error('Failed to initialize the git repository.');
      }

      // Check if the repo is for GitHub and create it using GitHub CLI if applicable
      if (repoUrl.includes('github.com')) {
        const repoName = getGitHubRepoName(repoUrl);

        // Check if GitHub CLI is installed
        if (shell.which('gh')) {
          console.log(`Creating GitHub repository ${repoName} using GitHub CLI...`);
          if (!runShellCommand(`gh repo create ${repoName} --public --source=. --remote=origin`, verbose)) {
            throw new Error('Failed to create GitHub repository.');
          }
        } else {
          console.log('GitHub CLI (gh) is not installed. Please ensure the repository exists on GitHub.');
        }
      }
      else {
        const gitRepoUrl = repoUrl.replace('https://', 'git@').replace('/', ':');
        if (!runShellCommand(`git remote add origin ${gitRepoUrl}`, verbose)) {
          throw new Error('Failed to add remote origin.');
        }
      }


      if (!runShellCommand('git add .', verbose)) {
        throw new Error('Failed to add files to the staging area.');
      }

      if (!runShellCommand('git commit -m "Initial commit"', verbose)) {
        throw new Error('Failed to create the initial commit.');
      }

      if (!runShellCommand(`git push --set-upstream origin ${branch}`, verbose)) {
        throw new Error('Failed to push the initial commit.');
      }

      console.log('Git repository initialized and initial commit pushed successfully.');
    }
  } catch (error) {
    console.error('Error initializing git repository:', error.message);
  }
};