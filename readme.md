# @fnet/gitlab-push-existing-folder

The primary purpose of the @fnet/gitlab-push-existing-folder project is to automate the process of initializing a Git repository within an existing directory, setting up the correct remote URL, and pushing the initial commit. The source code contains functionality for reading and parsing YAML files, executing shell commands, and interactive prompts for user confirmation.

## Main Functionality

The main function in this project is an asynchronous function named `index` which takes an object parameter `args`. This object could contain `repo_url`, a string indicating the repository URL; and `verbose`, a boolean value used to control the level of detail in command output.

### Initializing Git Repository and Committing

If the existing directory is not a Git repository, this software will initialize it as one. After that, it adds the repository as the remote origin and makes an initial commit. All these operations occur only after the user confirms their intent through an interactive prompt.

### Existing Git Repositories

In cases where the directory is already a Git repository, the software checks for and handles three specific scenarios:

1. **Same Remote URL:** If the existing remote URL is the same as the supplied one, the software lets the user know that the repository is already initialized with the same URL.
2. **Different Remote URL:** If the repository has a different remote URL than the provided one, the user is asked whether they want to update the remote URL to the new one.
3. **No Remote URL:** If the repository has no remote origin, the software prompts the user to confirm if they want to add the provided repository URL as the remote origin.

There are also several helper functions for running shell commands, verifying if the directory is a Git repository, retrieving the current remote URL, and loading and parsing a YAML file. 

## Error Handling

The software continuously checks whether each operation is successful. If a function fails, the software throws an error and halts its operation, ensuring that the user is always informed of any issues during setup. In every case, the user has the power to cancel any operation if they choose, making this tool robust and flexible in managing Git repositories. 

This project makes setting up and maintaining Git repositories easier and more straightforward, particularly in automated environments or for users unfamiliar with Git.